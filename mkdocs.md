# mkdocs + mkdocs-material

[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)
[MkDocs](https://www.mkdocs.org/)

## installation
```bash
poetry add --group dev mkdocs-material
```

Init project

Basic case:

```bash
mkdocs new .
```

I recommend use `docs_src` folder if you want publish docs in future.

```bash
mv docs docs_src
```

## Configuration mkdocs.yml


## Plugins
